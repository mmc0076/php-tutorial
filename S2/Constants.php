PHP Constants

A constant is an identifier (name) for a simple value. The value cannot be changed during the script.

Create a PHP Constant

define(name, value, case-insensitive)

<?php
define("GREETING", "How are u my friend.??");
echo GREETING;
?>

PHP Constant Arrays

<?php
define("cars", [
  "Alfa Romeo",
  "BMW",
  "Toyota"
]);
echo cars[0];
?>