//PHP tags

<?php echo 'this is first method '; ?>
<?= 'this is second method'; ?>

Escaping from HTML

<p>This is going to be ignored by PHP and displayed by the browser.</p>
<?php echo 'While this is going to be parsed.'; ?>
<p>This will also be ignored by PHP and displayed by the browser.</p>

Instruction separation 

<?php
    echo 'This is a test';
?>

<?php echo 'This is a test' ?>

Comments 

<?php
    echo 'This is a test'; // This is a one-line c++ style comment
    /* This is a multi line comment
       yet another line of comment */
    echo 'This is yet another test';
    echo 'One Final Test'; # This is a one-line shell-style comment
?>