Creating (Declaring) PHP Variables

<?php
$txt = "Hello world!";
$x = 5;
$y = 10.5;
?>

Output Variables

<?php
$txt = "programing";
echo "I love $txt!";
?>

<?php
$txt = "php";
echo "I love " . $txt . "!";
?>

<?php
$x = 5;
$y = 4;
echo $x + $y;
?>