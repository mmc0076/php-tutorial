PHP Functions

Create a User Defined Function in PHP

<?php
function writeMsg() {
  echo "Hello world!";
}

writeMsg(); // call the function
?>

PHP Function Arguments


<?php
function familyName($fname, $year) {
  echo "$fname Refsnes. Born in $year <br>";
}

familyName("Ali", "1975");
familyName("Zahra", "1978");
familyName("Mohammad", "1983");
?>

PHP Default Argument Value

<?php
function setHeight(int $minheight = 50) {
  echo "The height is : $minheight <br>";
}

setHeight(350);
setHeight(); // will use the default value of 50
setHeight(135);
setHeight(80);
?>

Passing Arguments by Reference

<?php
function add_five(&$value) {
  $value += 5;
}

$num = 2;
add_five($num);
echo $num;
?>